# Php

Php image for build and prod

## Usage of dev

Enable xdebug after composer install for better performance

```Dockerfile
FROM php-dev

RUN php composer.phar install -o -n

RUN docker-php-ext-enable xdebug
```
