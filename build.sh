#!/bin/sh

set -e
set -u

VERSION_BASE=$1
IMAGE=$CI_REGISTRY_IMAGE:${VERSION_BASE}-alpine

echo "Building ${IMAGE} with PHP_BASE=${VERSION_BASE}"
docker build -t ${IMAGE} -f Dockerfile --build-arg "PHP_BASE=${VERSION_BASE}" .
docker build -t ${IMAGE}-dev -f Dockerfile-xdebug --build-arg "PHP_BASE=${IMAGE}" .
docker push ${IMAGE}
docker push ${IMAGE}-dev
